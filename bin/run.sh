#!/bin/bash

pwd=`pwd`
if [ "`basename $pwd`" = "bin" ]
then
    echo "please, execute from the root folder. Like: ./bin/run.sh"
    exit 1;
fi

source env/bin/activate
python3 src/manage.py runserver 0.0.0.0:1596
