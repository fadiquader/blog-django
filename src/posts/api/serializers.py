from rest_framework.serializers import(
    ModelSerializer,
    SerializerMethodField,
    HyperlinkedIdentityField
)
from comments.api.serializers import (
    CommentListSerializer,
)
from accounts.api.serializers import UserDetailSerializer
from comments.models import Comment
from posts.models import Post

# post_detail_url = HyperlinkedIdentityField(
#     view_name='posts-api:detail',
# )


class PostSerializer(ModelSerializer):
    # url = post_detail_url
    # user = SerializerMethodField()
    user = UserDetailSerializer(read_only=True)

    class Meta:
        model = Post
        fields = [
            # 'url',
            'id',
            'user',
            'title',
            'content',
            'image',
            'publish'
        ]
    #
    # def get_user(self, obj):
    #     return str(obj.user)


class PostDetailSerializer(ModelSerializer):
    # user = SerializerMethodField()
    user = UserDetailSerializer(read_only=True)
    image = SerializerMethodField()
    comments = SerializerMethodField()
    class Meta:
        model = Post
        fields = [

            'id',
            'user',
            'title',
            'image',
            'slug',
            'content',
            'comments'
        ]

    # def get_user(self, obj):
    #     return str(obj.user)

    def get_image(self, obj):
        if obj.image:
            return obj.image.url
        return None

    def get_comments(self, obj):
        # content_type = obj.get_content_type
        # object_id = obj.object_id
        c_qs = Comment.objects.filter_by_instance(obj)
        comments = CommentListSerializer(c_qs, many=tuple).data
        return comments