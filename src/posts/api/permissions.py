from rest_framework.permissions import BasePermission, SAFE_METHODS


class IsOwnerOrReadOnly(BasePermission):

    message = 'You must be the owner of this post'
    #
    # def has_permission(self, request, view):
    #     my_safe_methods = ['GET', 'PUT'] # at this case we prevent delete method
    #     if request.method in my_safe_methods:
    #         return True
    #     return False

    def has_object_permission(self, request, view, obj):
        return obj.user == request.user
