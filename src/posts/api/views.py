from django.db.models import Q
from rest_framework.filters import (
    SearchFilter,
    OrderingFilter
)
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    RetrieveUpdateAPIView,
    DestroyAPIView,
    CreateAPIView
)
from rest_framework.pagination import (
    LimitOffsetPagination,
    PageNumberPagination
)
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    # IsAuthenticatedOrReadOnly,
    IsAdminUser
)
from .permissions import IsOwnerOrReadOnly

from .pagination import PostLimitOffsetPagination, PostPageNumberPagination
from .serializers import (
    PostSerializer,
    PostDetailSerializer,
)
from posts.models import Post


class PostDetailAPIView(RetrieveAPIView):
    queryset = Post.objects.all()
    permission_classes = [AllowAny]
    serializer_class = PostDetailSerializer
    # lookup_field = 'slug'
    # lookup_url_kwarg = 'slug'


class PostCreateAPIView(CreateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class PostDeleteAPIView(DestroyAPIView):
    queryset = Post.objects.all()
    serializer_class = PostDetailSerializer
    permission_classes = [IsOwnerOrReadOnly]


class PostUpdateAPIView(RetrieveUpdateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostDetailSerializer
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly]


class PostListAPIView(ListAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = [AllowAny]
    filter_backends = [SearchFilter]
    search_fields = ['title', 'content', 'user__first_name', 'user__last_name']
    # pagination_class = PageNumberPagination
    # pagination_class = PostLimitOffsetPagination # custom pagination ex. /?limit=2&offset=1
    # pagination_class = LimitOffsetPaginationPostPageNumberPagination # custom pagination ex. /page=2
    pagination_class = LimitOffsetPagination  # /?limit=2&offset=3

    # def get_queryset(self):
    #     # queryset = super(PostListAPIView, self).get_queryset(*args, **kwargs)
    #     queryset_list = Post.objects.all()
    #     query = self.request.GET.get('q')
    #     if query:
    #         queryset_list = queryset_list.filter(
    #             Q(title__icontains=query) |
    #             Q(content__icontains=query)|
    #             Q(user__first_name__icontains=query)|
    #             Q(user__last_name__icontains=query)
    #         ).distinct()

    # return queryset
