from django.contrib import admin
from .models import Post

class PostAdmin(admin.ModelAdmin):
    list_display = ["title", "timestamp", "updated"]
    list_display_links = ['timestamp']
    list_filter = ['updated', 'timestamp']
    list_editable = ['title']
    search_fields = ['title', 'content']
    # prepopulated_fields = {'slug': ('title',)}
    class Meta:
        model = Post
admin.site.register(Post, PostAdmin)
