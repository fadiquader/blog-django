from urllib.parse import quote_plus

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect, Http404
from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from comments.models import Comment
from comments.forms import CommentForm
from .models import Post
from .forms import PostForm
from django.contrib import messages
from django.utils import timezone

from .utils import get_read_time
# Create your views here.

@login_required(login_url='/login/')
def posts_create(request):
    if not request.user.is_staff or not request.user.is_superuser:
        return Http404

    if not request.user.is_authenticated():
        return Http404

    form = PostForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.user = request.user
        instance.save()
        messages.success(request, "Successfully created", extra_tags="html_safe")
        return HttpResponseRedirect(instance.get_absolute_url())
    else:
        messages.error(request, "Failed to create post")
    contect_dic = {
        "form": form
    }
    return render(request, 'posts/create.html', context=contect_dic)

def posts_detail(request, id=None):
    instance = get_object_or_404(Post, id=id) # Post.objects.get(id=1)
    if instance.publish > timezone.now().date():
        if not request.user.is_staff or not request.user.is_superuser:
            return Http404

    # content_type = ContentType.objects.get_for_model(Post)
    # obj_id = instance.id
    # comments = Comment.objects.filter(content_type=content_type, object_id = obj_id)
    initial_data = {
        "content_type": instance.get_content_type,
        "object_id": instance.id
    }
    form = CommentForm(request.POST or None)
    if form.is_valid():
        # c_type = form.cleaned_data.get('content_type')
        # obj_id = form.cleaned_data.get('object_id')
        content_data = form.cleaned_data.get('content')
        parent_object = None
        try:
            parent_id = int(request.POST.get('parent_id'))
        except:
            parent_id = None

        if parent_id:
            parent_qs = Comment.objects.filter(id=parent_id)
            if parent_qs.exists() and parent_qs.count() == 1:
                # parent_object = parent_qs.first()

                parent_object = parent_qs.first()

        new_comment, created = Comment.objects.get_or_create(
            user=request.user,
            content_type=instance.get_content_type,
            object_id=instance.id,
            content=content_data,
            parent=parent_object,
        )
        return HttpResponseRedirect(new_comment.content_object.get_absolute_url())
    print('read time', instance.read_time)
    context_dic = {
        "instance": instance,
        # "share_string": share_string,
        "comments": instance.comments,
        "comment_form": form
    }
    return render(request, 'posts/detail.html', context=context_dic)

def posts_list(request):
    today = timezone.now().date()
    queryset_list = Post.objects.active()
    if request.user.is_staff or request.user.is_superuser:
        queryset_list = Post.objects.all()
    query = request.GET.get('q')
    if query:
        queryset_list = queryset_list.filter(
            Q(title__icontains=query) |
            Q(content__icontains=query)|
            Q(user__first_name__icontains=query)|
            Q(user__last_name__icontains=query)
        ).distinct()

    paginator = Paginator(queryset_list, 10)
    page = request.GET.get('page')
    try:
        queryset = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        queryset = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        queryset = paginator.page(paginator.num_pages)

    context_dic = {
        "object_list": queryset,
        "today": today
    }
    return render(request, 'posts/post_list.html', context=context_dic)

@login_required(login_url='/login/')
def posts_update(request, id=None):
    if not request.user.is_staff or not request.user.is_superuser:
        return Http404

    if not request.user.is_authenticated():
        return Http404

    instance = get_object_or_404(Post, id=id)
    form = PostForm(request.POST or None, request.FILES or None, instance=instance)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.user = request.user
        instance.save()
        messages.success(request, "<a href='#'>Item saved", extra_tags="html_safe")
        return HttpResponseRedirect(instance.get_absolute_url())
    else:
        messages.error(request, "Failed to save item.", extra_tags="html_safe")
    context_dic = {
        "title": instance.title,
        "instance": instance,
        "form": form,
    }
    return render(request, 'posts/create.html', context=context_dic)

@login_required(login_url='/login/')
def posts_delete(request, id=None):
    instance = get_object_or_404(Post, id=id)
    instance.delete()
    return redirect("posts:list")
