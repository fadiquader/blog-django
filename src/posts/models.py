# from django.template.defaultfilters import slugify
from django.conf import settings
from django.core.urlresolvers import reverse
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models.signals import pre_save
from django.utils.text import slugify
from django.utils.crypto import get_random_string
from django.utils import timezone
from comments.models import Comment
from .utils import get_read_time

# Create your models here.

class PostManager(models.Manager):
    # Post.objects.all() = super(PostManager, self).all()
    def active(self, *args, **kwargs):
        return super(PostManager, self).filter(publish__lte=timezone.now())


def upload_location(instance, filename):
    filebase, extension = filename.split(".")
    # return "%s/%s.%s" %(instance.id, instance.id, extension)
    return "%s/%s" % (instance.id, filename)


class Post(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True)
    title = models.CharField(max_length=120)
    slug = models.SlugField(unique=True)
    image = models.ImageField(upload_to="images/",
                              null=True, blank=True, width_field="width_field",
                              height_field="height_field")
    height_field = models.IntegerField(default=0)
    width_field = models.IntegerField(default=0)
    content = models.TextField()
    draft = models.BooleanField(default=False)
    read_time = models.TimeField(null=True, blank=True, )
    publish = models.DateField(auto_now=False, auto_now_add=False)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)
    objects = PostManager()

    # def save(self, *args, **kwargs):
    #     slug = slugify(self.title)
    #     exists = Post.objects.filter(slug=slug).exists()
    #     if exists:
    #         slug = "%s%s" % (slug, self.id)
    #     self.slug = slug
    #     super(Post, self).save(*args, **kwargs)
    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("posts:detail", kwargs={"id": self.id})
        # return '/posts/detail/%s'%self.id

    def get_api_url(self):
        return reverse("posts-api:detail", kwargs={"id": self.id})
        # return '/posts/detail/%s'%self.id

    @property
    def comments(self):
        instance = self
        qs = Comment.objects.filter_by_instance(instance)
        return qs

    @property
    def get_content_type(self):
        instance = self
        content_type = ContentType.objects.get_for_model(instance.__class__)
        return content_type

    class Meta:
        ordering = ['-timestamp', '-updated']


def generate_slug_key(slug):
    exists = Post.objects.filter(slug=slug).exists()
    if exists:
        unique_id = str(get_random_string(length=10))
        slug += "-" + unique_id
        generate_slug_key(slug)
    return slug


def pre_save_post_receiver(sender, instance, *args, **kwargs):
    slug = slugify(instance.title)
    slug = generate_slug_key(slug)
    instance.slug = slug
    if instance.content:
        # read_time = get_read_time(instance.content)
        instance.read_time = get_read_time(instance.content)


pre_save.connect(pre_save_post_receiver, sender=Post)
