from ckeditor.widgets import CKEditorWidget
from django import forms
# from pagedown.widgets import PagedownWidget
from ckeditor.fields import RichTextField

from .models import Post

class PostForm(forms.ModelForm):
    # content = forms.CharField(widget=PagedownWidget)
    content = forms.CharField(widget=CKEditorWidget())
    publish = forms.DateField(widget=forms.SelectDateWidget)
    class Meta:
        model = Post
        fields = ['title',
                  'content',
                  'image',
                  'draft',
                  'publish'
                  ]