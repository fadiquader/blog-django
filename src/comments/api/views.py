from django.db.models import Q
from rest_framework.filters import (
    SearchFilter,
    OrderingFilter
)
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    RetrieveUpdateAPIView,
    DestroyAPIView,
    CreateAPIView
)
from rest_framework.mixins import DestroyModelMixin, UpdateModelMixin
from rest_framework.pagination import (
    LimitOffsetPagination,
    PageNumberPagination
)
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAuthenticatedOrReadOnly,
    IsAdminUser
)
from .serializers import (
    CommentListSerializer,
    CommentDetailSerializer,
    create_comment_serialzer,

)
from posts.api.serializers import (
    PostSerializer,
    PostDetailSerializer
)
from posts.api.pagination import PostPageNumberPagination
from posts.api.permissions import IsOwnerOrReadOnly
from comments.models import Comment
from posts.models import Post


class CommentListAPIView(ListAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentListSerializer
    permission_classes = [AllowAny]
    filter_backends = [SearchFilter]
    search_fields = ['content', 'user__first_name']
    pagination_class = PostPageNumberPagination


class CommentDetailAPIView(DestroyModelMixin, UpdateModelMixin, RetrieveAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentDetailSerializer
    lookup_field = 'pk'
    permission_classes = [IsOwnerOrReadOnly]
    # lookup_url_kwarg = 'slug'
    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class CommentCreateAPIView(CreateAPIView):
    queryset = Comment.objects.all()
    permission_classes = [IsAuthenticated]

    def get_serializer_class(self):
        model_type = self.request.GET.get('type') # EX. Post model, ...
        slug = self.request.GET.get('slug')
        parent_id = self.request.GET.get('parent_id', None)

        return create_comment_serialzer(model_type=model_type,
                                        slug=slug,
                                        parent_id=parent_id,
                                        user=self.request.user)


# class CommentEditAPIView(DestroyModelMixin, UpdateModelMixin, RetrieveAPIView):
#     queryset = Comment.objects.filter(id__gte=0)
#     permission_classes = [IsAuthenticated]
#     serializer_class = CommentDetailSerializer
#
#     def put(self, request, *args, **kwargs):
#         return self.update(request, *args, **kwargs)
#
#     def delete(self, request, *args, **kwargs):
#         return self.destroy(request, *args, **kwargs)


    # def perform_create(self, serializer):
    #     serializer.save(user=self.request.user)


# class PostDeleteAPIView(DestroyAPIView):
#     queryset = Post.objects.all()
#     serializer_class = PostDetailSerializer
#     permission_classes = [IsAuthenticatedOrReadOnly]
#
#
# class PostUpdateAPIView(RetrieveUpdateAPIView):
#     queryset = Post.objects.all()
#     serializer_class = PostDetailSerializer
#     permission_classes = [IsAuthenticated, IsOwnerOrReadOnly]
#
#



