from django.contrib.contenttypes.models import ContentType
from rest_framework.exceptions import ValidationError
from rest_framework.serializers import (
    ModelSerializer,
    SerializerMethodField,
    HyperlinkedIdentityField
)
from accounts.api.serializers import UserDetailSerializer
from django.contrib.auth import get_user_model

from comments.models import Comment

User = get_user_model()


# post_detail_url = HyperlinkedIdentityField(
#     view_name='posts-api:detail',
# )
#

class CommentListSerializer(ModelSerializer):
    user = UserDetailSerializer(read_only=True)
    replies = SerializerMethodField()
    reply_count = SerializerMethodField()
    # url = HyperlinkedIdentityField(view_name='comments-api:thread')

    class Meta:
        model = Comment
        fields = [
            'id',
            # 'url',
            # 'content_type',
            # 'object_id',
            # 'parent',
            'content',
            'user',
            'replies',
            'reply_count',
        ]

    # def get_user(self, obj):
    #     return str(obj.user)

    def get_replies(self, obj):
        if obj.is_parent:
            return CommentChildSerializer(obj.children, many=True).data
        return None

    def get_reply_count(self, obj):
        if obj.is_parent:
            return obj.children.count()
        else:
            return 0


class CommentChildSerializer(ModelSerializer):
    user = UserDetailSerializer(read_only=True)
    class Meta:
        model = Comment
        fields = [
            'id',
            'user',
            'content',
            'timestamp'
        ]


class CommentDetailSerializer(ModelSerializer):
    user = UserDetailSerializer(read_only=True)
    replies = SerializerMethodField()
    content_object_url = SerializerMethodField()

    class Meta:
        model = Comment
        fields = [
            'id',
            # 'content_type',
            # 'object_id',
            'user',
            'content',
            'timestamp',
            'replies',
            'content_object_url',
        ]
        read_only_fields = [
            'id',
            'object_id',
            'content_type',
            'replies'

        ]

    def get_replies(self, obj):
        if obj.is_parent:
            return CommentChildSerializer(obj.children, many=True).data
        return None

    def get_content_object_url(self, obj):
        try:
            return obj.content_object.get_api_url()
        except:
            return None


def create_comment_serialzer(model_type='post', slug=None, parent_id=None, user=None):
    class CommentCreateSerializer(ModelSerializer):
        class Meta:
            model = Comment
            fields = [
                'id',
                'content',
                'timestamp'
            ]

        def __init__(self, *args, **kwargs):
            self.model_type = model_type
            self.slug = slug
            self.parent_obj = None
            if parent_id:
                parent_qs = Comment.objects.filter(id=parent_id)
                if parent_qs.exists() and parent_qs.count() == 1:
                    self.parent_obj = parent_qs.first()
            return super(CommentCreateSerializer, self).__init__(*args, **kwargs)

        def validate(self, attrs):
            model_type = self.model_type
            model_qs = ContentType.objects.filter(model=model_type)
            print(model_qs)
            if not model_qs.exists() or model_qs.count() != 1:
                raise ValidationError("not valid content type")

            SomeModel = model_qs.first().model_class()
            obj_qs = SomeModel.objects.filter(slug=self.slug)

            if not obj_qs.exists() or obj_qs.count() != 1:
                raise ValidationError("not valid slug for this content type")

            return attrs

        def create(self, validated_data):
            content = validated_data.get('content')
            if user:
                main_user = user
            else:
                main_user = User.objects.all().first()
            model_type = self.model_type
            slug = self.slug
            parent_obj = self.parent_obj
            comment = Comment.objects.create_by_model_type(
                model_type=model_type,
                slug=slug,
                user=main_user,
                content=content,
                parent_obj=parent_obj
            )
            print('comment', comment)
            return comment

    # return the class not the instance
    return CommentCreateSerializer
