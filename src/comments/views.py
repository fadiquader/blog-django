from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.contrib import messages
from django.contrib.contenttypes.models import ContentType
from .models import Comment
from .forms import CommentForm


# Create your views here.
def comment_thread(request, id=None):
    try:
        obj = Comment.objects.get(id=id)
    except:
        raise Http404

    if not obj.is_parent:
        obj = obj.parent
    instance = obj.content_object
    # content_type =instance.get_content_type
    # object_id = obj.content_object.id
    form = CommentForm(request.POST or None)
    if form.is_valid() and request.user.is_authenticated():
        # c_type = form.cleaned_data.get('content_type')
        # obj_id = form.cleaned_data.get('object_id')
        content_data = form.cleaned_data.get('content')
        parent_object = None
        try:
            parent_id = int(request.POST.get('parent_id'))
        except:
            parent_id = None

        if parent_id:
            parent_qs = Comment.objects.filter(id=parent_id)
            if parent_qs.exists() and parent_qs.count() == 1:
                # parent_object = parent_qs.first()

                parent_object = parent_qs.first()

        new_comment, created = Comment.objects.get_or_create(
            user=request.user,
            content_type=instance.get_content_type,
            object_id=instance.id,
            content=content_data,
            parent=parent_object,
        )
        return HttpResponseRedirect(obj.get_absolute_url())

    context_dic = {
        "comment": obj,
        "comment_form": form
    }
    return render(request, 'comments/comment_thread.html', context=context_dic)


@login_required(login_url='/login/')
def comment_delete(request, id=None):
    try:
        obj = Comment.objects.get(id=id)
    except:
        raise Http404

    if obj.user != request.user:
        print('user doesn\'t have permission')
        response = HttpResponse('you don\'t have permission')
        response.status_code = 403
        raise response

    if request.method == 'POST':
        parent_object_url = obj.content_object.get_absolute_url()
        obj.delete()
        return HttpResponseRedirect(parent_object_url)

    context_dic = {
        "object": obj
    }
    return render(request, 'comments/confirm_delete.html', context=context_dic)
