
$(function () {
    $('.content-markdown').each(function () {
        var content = $(this).text();
        content = content.trim();
        var markedContent = marked(content);
        $(this).html(markedContent);
    });
    $('.post .content-markdown img').each(function () {
        $(this).fadeOut(10)
    });
    $('.comment-reply-btn').click(function (e) {
        e.preventDefault();
        $(this).parent().next('.comment-reply').slideToggle(200)
    })


        


});