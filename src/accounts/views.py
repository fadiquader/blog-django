from django.contrib.auth import (
    authenticate,
    login,
    logout
)
from django.shortcuts import render, redirect
from .forms import UserLoginForm, UserRegisterForm


# Create your views here.

def login_view(request):
    next_url = request.GET.get('next')
    title = 'Login'
    form = UserLoginForm(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        login(request, user)
        if next_url:
            return redirect(next_url)

        return redirect('/')

    context_dir = {
        "title": title,
        "form": form,
    }
    return render(request, 'accounts/form.html', context=context_dir)


def register_view(request):
    next_url = request.GET.get('next')
    title = 'Registration'
    form = UserRegisterForm(request.POST or None)
    if form.is_valid():
        user = form.save(commit=False)
        password = form.cleaned_data.get('password')
        user.set_password(password)
        user.save()
        new_user = authenticate(username=user.username, password=password)
        if new_user is not None:
            login(request, new_user)
            if next_url:
                return redirect(next_url)

            return redirect('/')
        else:
            print('auth error')

    context_dir = {
        'title': title,
        "form": form
    }
    return render(request, 'accounts/form.html', context=context_dir)


def logout_view(request):
    logout(request)
    return redirect('/')
    # return render(request, 'accounts/form.html', {})
