from django.conf.urls import url
from accounts.api.views import (
    UserCreateAPIView,
UserLoginAPIView
)
urlpatterns = [
    url(r'^register$', UserCreateAPIView.as_view(), name='register'),
    url(r'^login/$', UserLoginAPIView.as_view(), name='login'),
    # url(r'^(?P<pk>\d+)/$', UserCreateAPIView.as_view(), name='thread'),
    # # url(r'^list/$', views.posts_list, name='list'),
    # url(r'^(?P<pk>\d+)/edit/$',  CommentDetailAPIView.as_view(), name='edit'), # delete
    # url(r'^(?P<pk>\d+)/delete/$', PostDeleteAPIView.as_view(), name='delete'),

]
