from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from rest_framework.exceptions import ValidationError
from rest_framework.serializers import (
    ModelSerializer,
    SerializerMethodField,
    HyperlinkedIdentityField,
    EmailField,
    CharField
)
from django.contrib.auth import get_user_model

from comments.models import Comment

User = get_user_model()

class UserDetailSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'first_name',
            'last_name'
        ]

class UserCreateSerializer(ModelSerializer):
    # username = CharField(label='username', allow_blank=True) # to make the field optional
    email = EmailField(label='Email Address') # override the field to make to required
    email2 = EmailField(label='Confirm Email')

    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'email2',
            'password',
        ]

        extra_kwargs = {
            "password": {
                "write_only": True
            }
        }
    #
    # def validate(self, attrs): # general validation, will not execute of you used validation below
    #     email = attrs['email']
    #     user_qs = User.objects.filter(email=email)
    #     if user_qs.exsits():
    #         raise ValidationError('user name is already exists ..')
    #     return attrs

    def validate_email2(self, value):
        data = self.get_initial()
        email = data.get('email')
        email2 = data.get('email2')
        if email != email2:
            raise ValidationError('email must match')

        user_qs = User.objects.filter(email=email2)
        if user_qs.exists():
            raise ValidationError('user name is already exists ..')

        return value

    def create(self, validated_data):
        username = validated_data['username']
        email = validated_data['email']
        password = validated_data['password']
        user_obj = User(
            username=username,
            email=email
        )
        user_obj.set_password(password)
        user_obj.save()
        return validated_data


class UserLoginSerializer(ModelSerializer):
    token = CharField(label='username', allow_blank=True, read_only=True)
    username = CharField(required=False, allow_blank=True) # to make the field optional
    email = EmailField(label='Email Address')  # override the field to make to required

    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'password',
            'token',
        ]

        extra_kwargs = {
            "password": {
                "write_only": True
            }
        }

    def validate(self, attrs):
        email = attrs.get('email', None)
        username = attrs.get('username', None)
        password = attrs.get('password')
        if not email and not username:
            raise ValidationError('A username or password are required')
        user = User.objects.filter(
            Q(email=email) |
            Q(username=username)
        ).distinct().exclude(email__isnull=True)  # distinct to remove duplicated data
        if user.exists() and user.count() == 1:
            user_obj = user.first()
        else:
            raise ValidationError('This username/email is not valid')
        if user_obj:
            if not user_obj.check_password(password):
                raise ValidationError('Incorrect credentials')

        attrs["token"] = "some Random Token"
        return attrs