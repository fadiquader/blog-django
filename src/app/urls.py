"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.views.generic.base import TemplateView
from django.conf.urls.static import static
from rest_framework_jwt.views import obtain_jwt_token
from ang.views import AngularTemplateView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    # url(r'^comments/', include('comments.urls', namespace="comments")),
    # url(r'^', include('posts.urls', namespace="posts")),
    # url(r'^login/', login_view, name='login'),
    # url(r'^logout/', logout_view, name='logout'),
    # url(r'^register/', register_view, name='register'),
    url(r'^api/auth/token/', obtain_jwt_token),
    url(r'^api/posts/', include('posts.api.urls', namespace="posts-api")),
    url(r'^api/comments/', include('comments.api.urls', namespace="comments-api")),
    url(r'^api/users/', include('accounts.api.urls', namespace="users-api")),
    url(r'^api/templates/(?P<item>[A-Za-z0-9\_\-\.\/]+)\.html$',  AngularTemplateView.as_view()),

]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += [
    url(r'^$', TemplateView.as_view(template_name='ang/home.html')),
]
'''
    curl -X POST -d "username=fadiqua&password=F008390791" http://localhost:1596/api/auth/token/
    eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6ImZxQGdtYWlsLmNvbSIsImV4cCI6MTQ4Nzk2NzA5NiwidXNlcm5hbWUiOiJmYWRpcXVhIiwidXNlcl9pZCI6MX0.a5fyTpsEnA6rUU2NnZZ2SnXXLWcbRZEC-kBW2lTv1DY

    $ curl -H "Authorization: JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6ImZxQGdtYWlsLmNvbSIsImV4cCI6MTQ4Nzk2NzA5NiwidXNlcm5hbWUiOiJmYWRpcXVhIiwidXNlcl9pZCI6MX0.a5fyTpsEnA6rUU2NnZZ2SnXXLWcbRZEC-kBW2lTv1DY
        " http://localhost:1596/api/comments/

    $ curl http://localhost:1596/api/posts/

'''